﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Kahia.Common;
using Kahia.Common.Extensions.GeneralExtensions;
using Kahia.Common.Extensions.StringExtensions;

namespace PayuIpnBackHelper
{
    public static class PayuIPNHelper
    {
        public static readonly string SignatureKey = ConfigurationManager.AppSettings["Payu.Secret"].ThrowIfNullOrEmptyString("Payu.Secret AppSetting'i bulunamadı. Hashing için gereken key.");

        public static string HashWithSignatureHmacMd5(this string hashString)
        {
            var binaryHash = new HMACMD5(Encoding.UTF8.GetBytes(SignatureKey))
                .ComputeHash(Encoding.UTF8.GetBytes(hashString));

            var hash = BitConverter.ToString(binaryHash)
                .Replace("-", string.Empty)
                    .ToLowerInvariant();

            return hash;
        }

        public static string CalculateIpnBackEcho()
        {
            var request = HttpContext.Current.Request;
            var form = request.Form;
            var date = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            var hashBuilder = new List<string>
            {
                form["IPN_PID[0]"].ToStringByDefaultValue(form["IPN_PID[]"]),
                form["IPN_PNAME[]"],
                form["IPN_DATE"],
                date
            };
            var hashString = hashBuilder.Select(s => s.Length + s).JoinWith();
            var hashed = hashString.HashWithSignatureHmacMd5();
            return "<EPAYMENT>{0}|{1}</EPAYMENT><br/>OK".FormatString(date, hashed);
        }

        public static TPayuIPNBack HandleIPNRequest<TPayuIPNBack>()
            where TPayuIPNBack : IPayuIPNBack, new()
        {
            // debug için IPN requestini yönlendiriyoruz:
            //Bu sayfa bütün ipn requestleri için çalışacaktır. Ondan ötürü işlem özelinde ayrıştırmak gerekir.
            var ipAddress = System.Web.HttpContext.Current.FindIpAddressOfClient();

            var request = HttpContext.Current.Request;
            var form = request.Form.Count > 0 ? request.Form : request.QueryString;
            if (form.Count == 0)
                throw new HttpException((int)HttpStatusCode.NotFound, "no data posted!");

            var requestLog = new StringBuilder();
            foreach (string key in form)
                requestLog.AppendFormat("{0}:\t {1}\n", key, form[key]);

            //var context = DbContextManagerModule.GetDbContext<ClientEntities>();
            var ipnBack = new TPayuIPNBack
            {
                Commission = form["IPN_COMMISSION"],
                CreateDate = DateTime.Now,
                Currency = form["CURRENCY"],
                IpAddress = ipAddress,
                Price = form["IPN_PRICE[]"],
                ProductCode = form["IPN_PCODE[]"],
                Quantity = form["IPN_QTY[]"],
                RefNo = form["REFNO"],
                RefNoExt = form["REFNOEXT"],
                ReqeustLog = requestLog.ToString(),
                TokenHash = form["TOKEN_HASH"],
                Total = form["IPN_TOTAL[]"],
                PayuProductID = form["IPN_PID[]"],
            };
            return ipnBack;
        }
    }
}
