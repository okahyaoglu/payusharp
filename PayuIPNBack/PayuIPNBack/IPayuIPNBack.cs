﻿using System;

namespace PayuIpnBackHelper
{
    public interface IPayuIPNBack
    {
        string Commission { get; set; }
        DateTime CreateDate { get; set; }
        string Currency { get; set; }
        string IpAddress { get; set; }
        string Price { get; set; }
        string ProductCode { get; set; }
        string Quantity { get; set; }
        string RefNo { get; set; }
        string RefNoExt { get; set; }
        string ReqeustLog { get; set; }
        string TokenHash { get; set; }
        string Total { get; set; }
        string PayuProductID { get; set; }
    }

}
