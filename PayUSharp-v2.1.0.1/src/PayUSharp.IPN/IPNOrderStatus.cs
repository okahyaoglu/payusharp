using System.Xml.Serialization;

namespace PayUSharp2.IPN
{
    public enum IPNOrderStatus
    {
        [XmlEnum("PAYMENT_AUTHORIZED")]
        PaymentAuthorized,
        [XmlEnum("PAYMENT_RECEIVED")]
        PaymentReceived,
        [XmlEnum("TEST")]
        Test,
        [XmlEnum("CASH")]
        Cash,
        [XmlEnum("COMPLETE")]
        Complete,
        [XmlEnum("REVERSED")]
        Reversed,
        [XmlEnum("REFUND")]
        Refund
    }
    
}
