using System;

namespace PayUSharp2.Core
{
    public class PayuException: ApplicationException
    {
        public PayuException (string message, Exception innerException): base(message, innerException)
        {
        }
    }
}

