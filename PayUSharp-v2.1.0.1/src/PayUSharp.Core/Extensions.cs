﻿using PayUSharp2.Core.Base;

namespace PayUSharp2.Core
{
    public static class Extensions
    {
        public static void SetBillingDetails(this IPayuTrxParameters entity, BillingDetails billingDetails)
        {
            entity.BILL_ADDRESS = billingDetails.Address;
            entity.BILL_ADDRESS2 = billingDetails.Address2;
            entity.BILL_CITY = billingDetails.City;
            entity.BILL_COUNTRYCODE = billingDetails.CountryCode;
            entity.BILL_EMAIL = billingDetails.Email;
            entity.BILL_FAX = billingDetails.Fax;
            entity.BILL_FNAME = billingDetails.FirstName;
            entity.BILL_LNAME = billingDetails.LastName;
            entity.BILL_PHONE = billingDetails.PhoneNumber;
            entity.BILL_STATE = billingDetails.State;
            entity.BILL_ZIPCODE = billingDetails.ZipCode;
        }

    }
}
