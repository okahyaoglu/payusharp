﻿using PayUSharp2.Core;

namespace PayU2.AutomaticLiveUpdate
{
  public class ProductDetails: PayUSharp2.Core.Base.ProductDetails
  {
    [Parameter(Name = "ORDER_VER")]
    public string Version { get; set; }
  }
}
