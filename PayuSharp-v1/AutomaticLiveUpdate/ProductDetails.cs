﻿using System;
using System.Linq;
using System.Web.UI;
using Kahia.Common.Extensions.ReflectionExtensions;

namespace PayU.AutomaticLiveUpdate
{
    public class ProductDetails: Base.ProductDetails
    {
        [Parameter(Name = "ORDER_VER")]
        public string Version { get; set; }


        public override string ToString()
        {
            return $"{this.GetPropertyValuesAsKeyValueJointString(", ")} (base:'{base.ToString()}')";
        }


    }
}