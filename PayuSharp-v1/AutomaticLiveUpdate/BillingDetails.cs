﻿using Kahia.Common.Extensions.ReflectionExtensions;

namespace PayU.AutomaticLiveUpdate
{
    public class BillingDetails: Base.BillingDetails
    {
        public override string ToString()
        {
            return string.Format("{0} (base:'{1}')", this.GetPropertyValuesAsKeyValueJointString(", "), base.ToString());
        }
    }
}