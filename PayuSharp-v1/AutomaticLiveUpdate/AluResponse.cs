﻿using System;
using System.IO;
using System.Web;
using System.Xml.Serialization;
using System.Text;
using System.Xml;
using Kahia.Common.Extensions.ReflectionExtensions;

namespace PayU.AutomaticLiveUpdate
{
    public static class Statuses
    {
        public const string SUCCESS = "SUCCESS";
        public const string FAILED = "FAILED";
        public const string INPUT_ERROR = "INPUT_ERROR";
    }

    public static class ReturnCodes
    {
        public const string AUTHORIZED = "AUTHORIZED";
        public const string ThreeDSEnrolled = "3DS_ENROLLED";
        public const string ALREADY_AUTHORIZED = "ALREADY_AUTHORIZED";
        public const string AUTHORIZATION_FAILED = "AUTHORIZATION_FAILED";
        public const string INVALID_CUSTOMER_INFO = "INVALID_CUSTOMER_INFO";
        public const string INVALID_PAYMENT_INFO = "INVALID_PAYMENT_INFO";
        public const string INVALID_ACCOUNT = "INVALID_ACCOUNT";
        public const string INVALID_PAYMENT_METHOD_CODE = "INVALID_PAYMENT_METHOD_CODE";
        public const string INVALID_CURRENCY = "INVALID_CURRENCY";
        public const string REQUEST_EXPIRED = "REQUEST_EXPIRED";
        public const string HASH_MISMATCH = "HASH_MISMATCH";
        public const string NEW_ERROR = "NEW_ERROR";
        public const string INVALID_CURRENCY_FOR_SELLER = "INVALID_CURRENCY_FOR_SELLER";
    }

    [XmlRoot(ROOT_ELEMENT_NAME)]
    public class AluResponse
    {
        private const string ROOT_ELEMENT_NAME = "EPAYMENT";

        [XmlElement("REFNO")]
        public string RefNo { get; set; }

        [XmlElement("ALIAS")]
        public string Alias { get; set; }

        [XmlElement("STATUS")]
        public string Status { get; set; }

        [XmlElement("RETURN_CODE")]
        public string ReturnCode { get; set; }

        [XmlElement("RETURN_MESSAGE")]
        public string ReturnMessage { get; set; }

        [XmlElement("DATE")]
        public string Date { get; set; }

        [XmlElement("CURRENCY")]
        public string Currency { get; set; }

        [XmlElement("AMOUNT")]
        public decimal Total { get; set; }

        [XmlElement("INSTALLMENTS_NO")]
        public string InstallmentNumber { get; set; }

        [XmlElement("CARD_PROGRAM_NAME")]
        public string CardProgramName { get; set; }

        [XmlElement("URL_3DS")]
        public string Url3DS { get; set; }

        [XmlElement("HASH")]
        public string Hash { get; set; }

        [XmlElement("ORDER_REF")]
        public string ORDER_REF { get; set; }

        [XmlElement("IPN_CC_TOKEN")]
        public string TOKEN_ID { get; set; }

        [XmlElement("IPN_CC_MASK")]
        public string TOKEN_CC_LAST4DIGITS { get; set; }

        [XmlElement("IPN_CC_EXP_DATE")]
        public string TOKEN_CC_EXPRDATE { get; set; }

        public bool IsSuccess
        {
            get { return Status == Statuses.SUCCESS; }
        }

        public bool Is3DSReponse
        {
            get
            {
                return (ReturnCode == ReturnCodes.ThreeDSEnrolled &&
                        Url3DS != null &&
                        Url3DS != string.Empty);
            }
        }

        public static AluResponse FromString(string response)
        {
            try
            {
                using (var stringReader = new StringReader(response))
                    return new XmlSerializer(typeof(AluResponse)).Deserialize(stringReader) as AluResponse;
            }
            catch (Exception ex)
            {
                throw new Exception("aluResponse Parse exception. message: " + response, ex);
            }
        }

        public static AluResponse FromHttpRequest(HttpRequest request)
        {
            var xmlStr = ConvertRequestFormToXml(request);
            return FromString(xmlStr);
        }

        private static string ConvertRequestFormToXml(HttpRequest request)
        {
            StringBuilder xml = new StringBuilder();
            using (var writer = XmlWriter.Create(xml))
            {
                writer.WriteStartElement(ROOT_ELEMENT_NAME);
                foreach (var key in request.Form.AllKeys)
                {
                    writer.WriteStartElement(key);
                    writer.WriteString(request.Form[key]);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            return xml.ToString();
        }

        public override string ToString()
        {
            return string.Format("{0} (base:'{1}')", this.GetPropertyValuesAsKeyValueJointString(", "), base.ToString());
        }
    }
}