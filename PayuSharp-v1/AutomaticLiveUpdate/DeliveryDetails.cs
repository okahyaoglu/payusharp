using System;
using Kahia.Common.Extensions.ReflectionExtensions;

namespace PayU.AutomaticLiveUpdate
{
    public class DeliveryDetails: Base.DeliveryDetails
    {
        public override string ToString()
        {
            return string.Format("{0} (base:'{1}')", this.GetPropertyValuesAsKeyValueJointString(", "), base.ToString());
        }
    }
}

