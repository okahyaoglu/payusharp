﻿using PayU.Base;

namespace PayU.AutomaticLiveUpdate
{
    public class AluRequest : PayuRequestBase<PayuResponseBase>
    {
        public AluRequest(string merchant, OrderDetails order)
            : base(ParameterHandler.ParametersFromType(order))
        {
        }

        protected override PayuResponseBase CreatePayuResponse()
        {
            var response = base.CreatePayuResponse();
            response.PostedValues["ORDER_HASH"] = ParameterHandler.SignedHashString;
            return response;
        }

        public AluResponse ProcessPayment(OrderDetails obj, out PayuResponseBase payuResponse)
        {
            payuResponse = base.ProcessPayment();
            return AluResponse.FromString(payuResponse.ResponseString);
        }
    }
}