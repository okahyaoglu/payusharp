using System;
using Kahia.Common.Extensions.ReflectionExtensions;

namespace PayU.AutomaticLiveUpdate
{
    public class CardDetails
    {
        private string _expiryYear;
        private string _cardNumber;

        [Parameter(Name = "CC_NUMBER")]
        public string CardNumber
        {
            get { return _cardNumber; }
            set { _cardNumber = value.Trim().Replace("-","").Replace(" ",""); }
        }

        [Parameter(Name = "EXP_MONTH")]
        public string ExpiryMonth { get; set; }

        [Parameter(Name = "EXP_YEAR")]
        public string ExpiryYear
        {
            get { return _expiryYear; }
            set
            {
                _expiryYear = value; 
                if(!String.IsNullOrEmpty(value) && value.Length != 4)
                    throw new Exception("ExpiryYear YYYY formatında olmalıdır.");
            }
        }

        [Parameter(Name = "CC_TYPE")]
        public string CardType { get; set; }
        
        [Parameter(Name = "CC_CVV")]
        public string CVV { get; set; }
        
        [Parameter(Name = "CC_OWNER")]
        public string CardOwnerName { get; set; }

        public override string ToString()
        {
            return string.Format("{0} (base:'{1}')", this.GetPropertyValuesAsKeyValueJointString(", "), base.ToString());
        }
    }
}