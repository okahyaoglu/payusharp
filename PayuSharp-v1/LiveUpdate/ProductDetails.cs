using System;
using System.Collections.Generic;
using PayU.Base;

namespace PayU.LiveUpdate
{
    public class ProductDetails: PayU.Base.ProductDetails
    {
        public ProductDetails() {
            PriceType = PriceType.NET;
            Code = "";
            Name = "";
            Information = "";
        }

        [Parameter(Name = "ORDER_VAT", SortIndex = 46)]
        public decimal VAT { get; set; }
    }

}

