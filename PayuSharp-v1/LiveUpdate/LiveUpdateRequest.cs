using System;
using System.Text;

namespace PayU.LiveUpdate
{
    public class LiveUpdateRequest
    {
        private readonly string _signatureKey;
        public OrderDetails Order { get; private set; }
        private ParameterHandler ParameterHandler { get; set; }

        public LiveUpdateRequest(string signatureKey, OrderDetails order)
        {
            _signatureKey = signatureKey;
            Order = order;
            ParameterHandler = new ParameterHandler(ParameterHandler.ParametersFromType(order), false);
            ParameterHandler.CreateOrderRequestHash(signatureKey);
        }

        public string RenderPaymentForm(string buttonName)
        {
            return RenderPaymentForm(buttonName, "payForm");
        }

        public string RenderPaymentForm(string buttonName, string formId)
        {
            var sb = new StringBuilder();
            throw new NotImplementedException();
            //sb.AppendFormat(@"<form action=""{0}"" method=""POST"" id=""{1}"" name=""{2}""/>", Address, formId, formId);
            //sb.AppendLine();
            //sb.Append(RenderPaymentInputs());
            //sb.AppendFormat(@"<input type=""submit"" value=""{0}"">", buttonName);
            //sb.AppendLine();
            //sb.AppendLine("</form>");

            return sb.ToString();
        }

        public string RenderPaymentInputs()
        {
            var requestData = ParameterHandler.GetRequestData();

            var sb = new StringBuilder();

            foreach (var kvp in requestData)
            {
                sb.AppendFormat(@"<input type=""hidden"" name=""{0}"" value=""{1}"">", kvp.Key, kvp.Value);
                sb.AppendLine();
            }

            return sb.ToString();
        }

        public bool VerifyControlSignature(System.Web.HttpRequest request)
        {
            var ctrl = request.QueryString["ctrl"];

            if (string.IsNullOrEmpty(ctrl))
            {
                return false;
            }

            var url = request.Url.ToString().Replace("&ctrl=" + ctrl, "").Replace("?ctrl=" + ctrl, "");

            var hashString = url.Length.ToString() + url;
            var hash = hashString.HashWithSignature(_signatureKey);

            return hash == ctrl.ToLowerInvariant();
        }
    }
}

