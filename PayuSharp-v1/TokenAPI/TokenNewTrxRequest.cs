﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using PayU.AutomaticLiveUpdate;
using PayU.Base;
using PayU.MarketPlaceApi;
using BillingDetails = PayU.Base.BillingDetails;
using ProductDetails = PayU.Base.ProductDetails;

namespace PayU.TokenAPI
{
    public class TokenNewTrxRequest : PayuRequestBase<TokenTrxResponse>
    {
        public TokenNewTrxRequest(IPayuTrxParameters requestParameters, string CC_TOKEN, IEnumerable<ProductDetails> products, string subMerchantName)
            : base(requestParameters.GetPropertyValuesAsValueHolderCollection(typeof(IPayuTrxParameters)))
        {
            Parameters["PAY_METHOD"] = "CCVISAMC";
            Parameters["LU_ENABLE_TOKEN"] = "1";
            Parameters["ORDER_DATE"] = TimeStampDate.ToPayuString();
            Parameters["CC_NUMBER"] = ""; //.ToValueHolder();
            Parameters["EXP_MONTH"] = ""; //.ToValueHolder();
            Parameters["EXP_YEAR"] = ""; //.ToValueHolder();
            Parameters["CC_CVV"] = ""; //.ToValueHolder();
            Parameters["CC_OWNER"] = ""; //.ToValueHolder();
            Parameters["CC_TOKEN"] = CC_TOKEN; //.ToValueHolder();

            var index = 0;
            foreach (var product in products)
            {
                Parameters["ORDER_MPLACE_MERCHANT[" + index + "]"] = subMerchantName;
                Parameters["ORDER_PCODE[" + index + "]"] = product.Code;
                Parameters["ORDER_PNAME[" + index + "]"] = product.Name;
                Parameters.Add("ORDER_PRICE[" + index + "]", product.UnitPrice.RoundString());
                Parameters["ORDER_PRICE_TYPE[" + index + "]"] = product.PriceType == PriceType.GROSS ? "GROSS" : "NET";
                Parameters["ORDER_QTY[" + index + "]"] = product.Quantity.ToString();
                Parameters["ORDER_VAT[" + index + "]"] = "0";
                index++;
            }
        }

        protected override TokenTrxResponse CreatePayuResponse()
        {
            var response = base.CreatePayuResponse();
            response.PostedValues.Add("ORDER_HASH", ParameterHandler.SignedHashString);
            return response;
        }

        protected override void ProcessRequest(TokenTrxResponse result)
        {
            base.ProcessRequest(result);
            if (result.ResponseString == null)
                return;

            var xdoc = XDocument.Parse(result.ResponseString);
            var _dictionary = xdoc.Descendants().ToDictionary(d => d.Name.LocalName, d => d.Value);
            result.Parameters = _dictionary;
        }

    }
}
