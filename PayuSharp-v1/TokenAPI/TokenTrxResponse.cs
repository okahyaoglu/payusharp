﻿
using System.Collections.Generic;
using Kahia.Common.Extensions.CollectionExtensions;
using Kahia.Common.Extensions.StringExtensions;
using PayU.Base;

namespace PayU.TokenAPI
{
    /*
<?xml version="1.0"?>
<EPAYMENT>
<REFNO>27231136</REFNO>
<ALIAS>2d7539335342217c966fd14921cbb158</ALIAS>
<STATUS>SUCCESS</STATUS>
<RETURN_CODE>3DS_ENROLLED</RETURN_CODE>
<RETURN_MESSAGE>3DS Enrolled Card.</RETURN_MESSAGE>
<DATE>2017-02-20 14:19:38</DATE>
<URL_3DS>https://secure.payu.com.tr/order/3ds/begin/refno/27231136/sign/0c40b4f03178f3d15027c42783f6e005/</URL_3DS>
<HASH>b4326d7970a607f359956cf18b39de5f</HASH>
</EPAYMENT>
  */
    public class TokenTrxResponse : PayuResponseBase
    {
        public Dictionary<string, string> Parameters { get; set; }
        public string REFNO { get { return Parameters.TryGet("REFNO"); } }
        public string STATUS { get { return Parameters.TryGet("STATUS"); } }
        public string RETURN_CODE { get { return Parameters.TryGet("RETURN_CODE"); } }
        public string RETURN_MESSAGE { get { return Parameters.TryGet("RETURN_MESSAGE"); } }
        public string URL_3DS { get { return Parameters.TryGet("URL_3DS"); } }

        public bool IsSuccessful()
        {
            return STATUS.Equals("SUCCESS");
        }

        public bool Is3ds()
        {
            return IsSuccessful() && RETURN_CODE.Equals("3DS_ENROLLED");
        }

        public override string ToString()
        {
            return "Response Parameters: " + Parameters.GetCollectionString() + "\n" + base.ToString();
        }
    }
}
