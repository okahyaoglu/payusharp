﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using PayU.AutomaticLiveUpdate;
using PayU.Base;
using PayU.MarketPlaceApi;
using BillingDetails = PayU.Base.BillingDetails;
using ProductDetails = PayU.Base.ProductDetails;

namespace PayU.TokenAPI
{
    public class TokenCreateRequest : PayuRequestBase<TokenTrxResponse>
    {
        public TokenCreateRequest(IPayuTrxParameters requestParameters, CardDetails cardinfo, IEnumerable<ProductDetails> products, string subMerchantName = null)
            : base(requestParameters.GetPropertyValuesAsValueHolderCollection(typeof(IPayuTrxParameters)))
        {
            Parameters["PAY_METHOD"] = "CCVISAMC"; //.ToValueHolder();
            Parameters["LU_ENABLE_TOKEN"] = "1"; //.ToValueHolder();
            Parameters["ORDER_DATE"] = TimeStampDate.ToPayuString(); //.ToValueHolder();
            Parameters["CC_NUMBER"] = cardinfo.CardNumber; //.ToValueHolder();
            Parameters["CC_OWNER"] = cardinfo.CardOwnerName; //.ToValueHolder();
            Parameters["EXP_MONTH"] = cardinfo.ExpiryMonth; //.ToValueHolder();
            Parameters["EXP_YEAR"] = cardinfo.ExpiryYear; //.ToValueHolder();
            Parameters["CC_CVV"] = cardinfo.CVV; //.ToValueHolder();

            var index = 0;
            foreach (var product in products)
            {
                if (subMerchantName != null)
                    Parameters["ORDER_MPLACE_MERCHANT[" + index + "]"] = subMerchantName; //.ToValueHolder();
                Parameters["ORDER_PCODE[" + index + "]"] = product.Code; //.ToValueHolder();
                Parameters["ORDER_PNAME[" + index + "]"] = product.Name; //.ToValueHolder();
                Parameters["ORDER_PRICE[" + index + "]"] = product.UnitPrice.RoundString();
                Parameters["ORDER_PRICE_TYPE[" + index + "]"] = (product.PriceType == PriceType.GROSS ? "GROSS" : "NET"); //.ToValueHolder();
                Parameters["ORDER_QTY[" + index + "]"] = product.Quantity.ToString(); //.ToValueHolder();
                Parameters["ORDER_VAT[" + index + "]"] = "0"; //.ToValueHolder();
                index++;
            }
        }

        protected override TokenTrxResponse CreatePayuResponse()
        {
            var response = base.CreatePayuResponse();
            response.PostedValues.Add("ORDER_HASH", ParameterHandler.SignedHashString);
            return response;
        }

        protected override void ProcessRequest(TokenTrxResponse result)
        {
            base.ProcessRequest(result);
            if (result.ResponseString == null)
                return;

            var xdoc = XDocument.Parse(result.ResponseString);
            var _dictionary = xdoc.Descendants().ToDictionary(d => d.Name.LocalName, d => d.Value);
            result.Parameters = _dictionary;
        }

    }
}
