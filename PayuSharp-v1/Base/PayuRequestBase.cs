﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Kahia.Common.Extensions.CollectionExtensions;
using Kahia.Common.Extensions.StringExtensions;
using Kahia.Web.Extensions;

namespace PayU.Base
{
    public class PayuResponseBase
    {
        public string RequestURL;
        public HttpMethod RequestMethod;
        public Dictionary<string, string> PostedValues;
        public string ResponseString;
        public string HashString;

        public override string ToString()
        {
            return $"URL: \n{RequestURL}\nMethod: \n{RequestMethod}\nParameters: \n{PostedValues.GetCollectionString()}Hash String: \n{HashString}\nResponse: \n{ResponseString}";
        }
    }


    public class PayuRequestBase<TResponse> where TResponse : PayuResponseBase, new()
    {
        public Dictionary<string, string> Parameters { get; set; }
        public ParameterHandler ParameterHandler { get; set; }

        public string TimeStamp { get; private set; }
        public DateTime TimeStampDate { get; private set; }
        public StringBuilder LogBuilder { get; private set; }

        public PayuRequestBase(Dictionary<string, string> parameters)
        {
            LogBuilder = new StringBuilder();
            TimeStampDate = DateTime.UtcNow;
            TimeStamp = TimeStampDate.ToUnixTimestamp().ToString();
            Parameters = parameters ?? new Dictionary<string, string>();
        }

        protected virtual void CreateParameterHandler(Dictionary<string, string> parameters)
        {
            ParameterHandler = new ParameterHandler(parameters, TimeStampDate);
        }

        public virtual TResponse ProcessPayment()
        {
            CreateParameterHandler(Parameters);
            ParameterHandler.CreateOrderRequestHash();
            var response = SendRequest();
            return response;
        }

        public bool Validator(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private TResponse SendRequest()
        {
            var result = CreatePayuResponse();
            ProcessRequest(result);
            return result;
        }

        protected virtual TResponse CreatePayuResponse()
        {
            var result = new TResponse
                         {
                             RequestURL = "https://secure.payu.com.tr/order/alu/v3",
                             PostedValues = ParameterHandler.GetRequestData(),
                             RequestMethod = HttpMethod.Post,
                             HashString = ParameterHandler.HashString
                         };
            return result;
        }

        protected virtual void ProcessRequest(TResponse result)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = Validator;
                using (var client = new HttpClient(new LoggingHandler(new HttpClientHandler(), LogBuilder)))
                {
                    Debug.WriteLine("[PAYU REQUEST]\n{0} {1}\n{2}", result.RequestMethod, result.RequestURL, result.PostedValues.GetCollectionString());
                    if (result.RequestMethod == HttpMethod.Get)
                    {
                        Array.ForEach(result.PostedValues.ToArray(), kvp => result.RequestURL = result.RequestURL.AppendQueryString(kvp.Key, kvp.Value));
                        result.PostedValues = new Dictionary<string, string>();
                        result.ResponseString = Task.Run(() => client.GetStringAsync(result.RequestURL)).Result;
                    }
                    else
                    {
                        var bytes = Task.Run(() => client.PostAsync(result.RequestURL, new FormUrlEncodedContent(result.PostedValues))).Result;
                        var uploadResult = Task.Run(() => bytes.Content.ReadAsByteArrayAsync()).Result;
                        result.ResponseString = Encoding.UTF8.GetString(uploadResult);
                    }
                    Debug.WriteLine("[PAYU RESPONSE]\n" + result.ResponseString);
                    if (result.ResponseString == "Invalid signature!")
                        throw new Exception("Payu'dan Invalid Signature hatası geldi. Response:" + result.ResponseString);
                }
            }
            catch (WebException ex)
            {
                throw new PayuException(
                    $"Request to url {result.RequestURL} failed with status {ex.Status} and response {ex.Response}", ex);
            }
            catch (Exception ex)
            {
                throw new PayuException("An exception occured during ALU request", ex);
            }
            //var webClient = new WebClient();
            //webClient.Encoding = Encoding.UTF8;
            //try
            //{
            //    if (result.RequestMethod == HttpMethod.Get)
            //    {
            //        Array.ForEach(result.PostedValues.ToArray(), kvp => result.RequestURL = result.RequestURL.AppendQueryString(kvp.Key, kvp.Value));
            //        result.PostedValues = new Dictionary<string, string>();
            //        result.ResponseString = webClient.DownloadString(result.RequestURL);
            //    }
            //    else
            //    {
            //        var uploadResult = webClient.UploadValues(result.RequestURL, result.RequestMethod.Method, result.PostedValues.ToNameValueCollection());
            //        result.ResponseString = Encoding.UTF8.GetString(uploadResult);
            //    }
            //    return result;
            //}
            //catch (WebException ex)
            //{
            //    throw new PayuException(string.Format("Request to url {0} failed with status {1} and response {2}", result.RequestURL, ex.Status, ex.Response), ex);
            //}
            //catch (Exception ex)
            //{
            //    throw new PayuException("An exception occured during ALU request", ex);
            //}
        }
    }

}
