using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Kahia.Common.Extensions.ReflectionExtensions;

namespace PayU.Base
{
    public class OrderDetails
    {
        public OrderDetails()
        {
            PaymentMethod = "CCVISAMC";
            OrderHash = "";
            OrderDate = DateTime.UtcNow;
            PricesCurrency = "TRY";
        }

        /* Additional parameters */
        //[Parameter(Name = "TESTORDER", ExcludeFromHash = true)]
        //public bool? TestOrder { get; set; }

        //[Parameter(Name = "DEBUG", ExcludeFromHash = true)]
        //public bool? Debug { get; set; }

        /* Required Parameters */
        [Parameter(Name = "MERCHANT", SortIndex = 10)]
        public string Merchant { get; set; }
        
        [Parameter(Name = "ORDER_REF", SortIndex = 20)]
        public string OrderRef { get; set; }
        
        [Parameter(Name = "ORDER_DATE", FormatString = "{0:yyyy-MM-dd HH:mm:ss}", SortIndex = 30)]
        internal DateTime OrderDate { get; set; }
        
        [Parameter(Name = "PAY_METHOD", SortIndex = 110)]
        public string PaymentMethod { get; set; }

        [Parameter(Name = "ORDER_HASH")] //, ExcludeFromHash = true
        public string OrderHash { get; protected set; }
        
        [Parameter(Name = "PRICES_CURRENCY", SortIndex = 60)]
        public string PricesCurrency { get; set; }


        public override string ToString()
        {
            return $"{this.GetPropertyValuesAsKeyValueJointString(", ")}";
        }

    }
}

