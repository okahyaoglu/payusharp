﻿using System;
using System.Web;
using System.Web.UI;

namespace PayU
{
    public static class OrderManager
    {
        public const string ValidationGroup = "Checkout";

        public static int? CurrentOrderID
        {
            get { return HttpContext.Current.Session["CurrentOrder"] as int?; }
            set { HttpContext.Current.Session["CurrentOrder"] = value; }
        }

        public static void DisableCacheAndAutoComplete(Page page)
        {
            var response = page.Response;
            response.Cache.SetAllowResponseInBrowserHistory(false);
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.SetNoStore();
            response.Cache.SetExpires(DateTime.Now);
            response.Cache.SetValidUntilExpires(true);

            page.Form.Attributes["autocomplete"] = "off";
        }

        public static void PrecheckCurrentOrder(Page page)
        {
            if (CurrentOrderID == null) //order boş olamaz artık burada.
                page.RedirectTo(page.LinkCreator.MainPage().AppendQueryString("message", "orderempty"), true);
        }

        public static void PrecheckOrderStatusAndRedirectIfRequired(Page page, Order order = null)
        {
            //eğer tamamlanmışsa satın alma işlemi, buralara düşmemesi lazım tekrardan
            if (order == null)
                using (var context = new UIEntities())
                    order = FetchOrderFromDb(context);

            if (order.Status == OrderStatus.Success)
            {
                CurrentOrderID = null;
                page.RedirectTo(page.LinkCreator.MainPage().ResolveCurrencyUrl());
            }
        }

        public static Order FetchOrderFromDb(UIEntities context, int? orderid = null, bool hideExceptions = false)
        {
            orderid = orderid ?? CurrentOrderID;
            if (!hideExceptions)
                orderid.ThrowIfNull("orderid != null");
            var order = context.Orders
                .Include("Product")
                .Include("User")
                .Include("UserAddress")
                //.Include("OrderPayment")
                .Include("Currency")
                .FirstOrDefault(o => o.OrderID == orderid);
            if (!hideExceptions)
                order.ThrowIfNull("'{0}' idli order bulunamadı.", orderid);
            return order;
        }

        public static void CreateNewOrderAndSave(Func<Order> modifyOrderFunc)
        {
            using (var context = new UIEntities())
            {
                var order = modifyOrderFunc();
                context.Orders.Add(order);
                context.SaveChanges();
                CurrentOrderID = order.OrderID;
            }
        }


        public static void PrecheckOrderUserID(Page page)
        {
            using (var context = new UIEntities())
            {
                var order = FetchOrderFromDb(context);

                if (order.FK_UserID == null)
                    order.FK_UserID = UIAuthManager.Instance.CurrentUserID;
                context.SaveChanges();
            }
        }
    }
}
