﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Instack.Common;
using Instack.Common.Application.UIFormBase;
using Instack.Common.DataAccess;
using Kahia.Common;
using Kahia.Common.Extensions.ConversionExtensions;
using Kahia.Common.Extensions.StringExtensions;
using Kahia.Web.Currency;
using Kahia.Web.Extensions;
using Kahia.Web.Interfaces;
using PayU;
using PayU.AutomaticLiveUpdate;

namespace Instack.UI.Project
{
    public partial class Checkout3dsBack : UIWebFormBase, INotHtmlPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var aluResponse = AluResponse.FromHttpRequest(Request);
            var isSuccess = aluResponse.Status == Statuses.SUCCESS && aluResponse.ReturnCode == ReturnCodes.AUTHORIZED;
            var orderID = aluResponse.ORDER_REF.ToNullableInt();
            string currencyCode = "";
            if (aluResponse.Currency.IsNotNullAndEmptyString())
                currencyCode = aluResponse.Currency.ToUpperInvariant();

            using (var context = new UIEntities())
            {
                if (orderID == null)
                {
                    var orderPayment = new Order();
                    orderPayment.Payu3dsResponse = aluResponse.ToString();
                    orderPayment.PayuRefNumber = aluResponse.RefNo;
                    orderPayment.PayuResponseDate = Extensions.Now();
                    orderPayment.Status = OrderStatus.PayuSuccessWithoutOrder; //bunu flag olarak da kullanıyorum. böyle bir senaryo olmaması laızm normalde
                    context.Orders.Add(orderPayment);
                }
                else
                {
                    var order = OrderManager.FetchOrderFromDb(context, orderID);
                    order.PayuResponse = order.PayuResponse.Append(Environment.NewLine).Append(aluResponse.ToString());
                    order.PayuRefNumber = aluResponse.RefNo;
                    if (currencyCode.IsNotNullAndEmptyString())
                        order.Currency.Code = currencyCode;

                    if (isSuccess)
                    {
                        order.Product.CountStacked++;
                        order.IsPaymentApproved = true;
                        order.Status = OrderStatus.Success;
                        OutputCacheHelper.ReleaseCache();
                    }
                    else
                    {
                        order.Status = OrderStatus.PaymentRejectedFromPayu;
                    }
                }
                context.SaveChanges();
            }
            if (isSuccess)
                RedirectTo(LinkCreator.Checkout3().ResolveUrl().AppendQueryString(CurrencyWebFormManager.CurrencyQueryString, currencyCode));
            else
            {
                RedirectTo(LinkCreator.Checkout2().ResolveUrl()
                    .AppendQueryString(CurrencyWebFormManager.CurrencyQueryString, currencyCode)
                    .AppendQueryString("3DSerrorcode", aluResponse.ReturnCode)
                    .AppendQueryString("3DSerror", aluResponse.ReturnMessage));
            }
        }

        public override string GetVirtualUrl(string langCode)
        {
            return "~/Project/Checkout3dsBack.aspx".AppendQueryString("l", langCode);
        }
    }
}