﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Instack.Common;
using Instack.Common.Application.UIFormBase;
using Instack.Common.DataAccess;
using Kahia.Common;
using Kahia.Common.Extensions.ConversionExtensions;
using Kahia.Common.Extensions.StringExtensions;
using Kahia.Web.Extensions;
using Kahia.Web.Helpers;
using PayU;

namespace Instack.UI.Project
{
    public partial class Checkout1 : UIWebFormBase
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            #region Checkout Page Checks
            OrderManager.DisableCacheAndAutoComplete(this);
            CheckLoginAndRedirectIfNotLoggedIn();
            OrderManager.PrecheckCurrentOrder(this);
            OrderManager.PrecheckOrderStatusAndRedirectIfRequired(this);
            OrderManager.PrecheckOrderUserID(this);

            //todo: state yönetilmeli orderın, payudan cevap beklerken buraya gelirse napcak?
            #endregion
            
            FormHelper.UpdateValidatorsCommonAttributes(true, ValidatorDisplay.Dynamic, true, OrderManager.ValidationGroup, pnlForm, btnSubmit);
            FormHelper.RegisterValidateAndOverlayScripts(OrderManager.ValidationGroup, GetType(), btnSubmit, ".inner .form-overlay");
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ddlAddress.Items.Clear();
            var userid = UIAuthManager.Instance.CurrentUserID;
            using (var context = new UIEntities())
            {
                var addresses = context.UserAddresses.Where(ua => ua.FK_UserID == userid.Value).OrderByDescending(ua => ua.LastUpdateDate).ToArray();
                Array.ForEach(addresses, ua => ddlAddress.Items.Add(new ListItem("{0}: {1}".FormatString(ua.NameSurname, ua.Address.Left(50)), ua.UserAddressID.ToString())));
            }
            ddlAddress.Items.Add(new ListItem("Add new address", ""));
        }

        public override string GetVirtualUrl(string langCode)
        {
            return LinkCreator.Checkout1(langCode);
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            Page.Validate(OrderManager.ValidationGroup);
            if (!Page.IsValid)
                return;

            var userid = UIAuthManager.Instance.CurrentUserID;
            using (var context = new UIEntities())
            {
                //buradayken orderid dolu gelmeli fakat address boş gelmeli. savechangestan sonra useradress dolu gelmeli.
                var order = OrderManager.FetchOrderFromDb(context);
                var address = new UserAddress
                              {
                                  NameSurname = GetText(txtFirstName) + " " + GetText(txtLastName),
                                  Address = GetText(txtAddress1),
                                  Address2 = GetText(txtAddress2),
                                  ZipCode = GetText(txtZipCode),
                                  City = GetText(txtCity),
                                  LastUpdateDate = Extensions.Now(),
                                  FK_UserID = userid.Value,
                                  UpdateIPAddress = Context.FindIpAddressOfClient()
                              };
                order.UserAddress = address;
                context.SaveChanges();
            }
            Response.Redirect(LinkCreator.Checkout2().ResolveCurrencyUrl());
        }

        private string GetText(TextBox textBox)
        {
            return textBox.Text.Trim();
        }

        protected void btnUseThisAddress_OnClick(object sender, EventArgs e)
        {
            var addressID = ddlAddress.SelectedValue.ToInt();
            using (var context = new UIEntities())
            {
                var order = OrderManager.FetchOrderFromDb(context);
                order.FK_UserAddressID = addressID;
                context.SaveChanges();
                if (order.FK_UserID != UIAuthManager.Instance.CurrentUserID)
                    throw new Exception("Security exception: userids mismatch");
            }
            Response.Redirect(LinkCreator.Checkout2().ResolveCurrencyUrl());
        }
    }
}