﻿<%@ Page HtmlCssClass="Checkout3-page Checkout-page" MasterPageFile="~/MasterPages/TwoCards.master" CodeBehind="Checkout3.aspx.cs" Inherits="Instack.UI.Project.Checkout3" AutoEventWireup="true" Language="C#" %>

<%@ MasterType VirtualPath="~/MasterPages/TwoCards.master" %>
<%@ Register Src="~/Project/UserControls/ucSocialShare.ascx" TagPrefix="uc1" TagName="ucSocialShare" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <script>
        $(function() {
            $('.right .image').clone().removeClass('image').addClass('image2').insertBefore('.page-container .card .right');
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeCards" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeftCard" runat="server">
    <ul class="tabs tab-3">
        <li>
            <a><small>Step 1</small><span>Address</span></a>
        </li>
        <li>
            <a><small>Step 2</small><span>Payment</span></a>
        </li>
        <li>
            <a class="active"><small>Step 3</small><span>Complete</span></a>
        </li>
    </ul>
    <div class="inner">
        <div class="info">
            <div class="complete-icon">
                <i class="fa fa-check"></i>
            </div>
            <h2>Thank you for your purchase!</h2>
            <p>You are the <strong><%=CurrentOrder.NthStack %>th</strong> person want this stack.<br />
                Collect <%=ProductStacked.CountRequired - ProductStacked.CountStacked %> more stacks to make it happen.
            </p>
            <p>Don't forget to spread the word by sharing your stack!</p>
        </div>
        <div>
            <input type="button" value="Share it" class="btn btn-submit" onclick="$('.social-share').show();" />
            <uc1:ucSocialShare runat="server" CssClass="social-share" ID="SocialShare" />
            <p class="count-required">Need <b><%= ProductStacked.CountRequired - ProductStacked.CountStacked %></b> more to produce</p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphRightCard" runat="server">
    <asp:PlaceHolder runat="server" ID="plcRelated">
        <h3>Check these also</h3>
        <div class="image" style="<%# RelatedProduct.GetImage("m").GetFullResolvedUrlWithPort().ToBackgroundImage() %>"></div>
        <div class="status">
            <h1><%# Translated(RelatedProduct.FT_Title) %></h1>
            <span class="current"><strong><%# RelatedProduct.CountStacked %></strong> / <%# RelatedProduct.CountRequired %></span>
        </div>
        <div class="total">
            <span><%# RelatedProduct.Price %></span>
            <input type="button" value="Stack it" class="btn" onclick="location.href='<%# LinkCreator.ProductDetail(RelatedProduct).ResolveCurrencyUrl()%>    ';" />
        </div>
        <div class="btn-more">
            <input type="button" value="Show more" class="btn" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterCards" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphFooterScripts" runat="server">
</asp:Content>
