﻿<%@ Page HtmlCssClass="Checkout2-page Checkout-page" MasterPageFile="~/MasterPages/TwoCards.master" CodeBehind="Checkout2.aspx.cs" Inherits="Instack.UI.Project.Checkout2" AutoEventWireup="true" Language="C#" %>

<%@ MasterType VirtualPath="~/MasterPages/TwoCards.master" %>
<%@ Register Src="~/Project/UserControls/ucCheckoutSelectedProduct.ascx" TagPrefix="uc1" TagName="ucCheckoutSelectedProduct" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        //valid classı gelmeli validse textbox
        var checkoutPanel = '#<%=pnlForm.ClientID%>';
        var checkoutBtn = '#<%=btnSubmit.ClientID%>';
        $(function () {
            tools.inputFilters.allowOnlyNumbers('#<%=txtCardNumber.ClientID%>,#<%=txtEndDate.ClientID%>,#<%=txtCvc.ClientID%>');
            $(checkoutBtn).on('form-validated', function () {
                $('ul li', checkoutPanel).each(function (index, li) {
                    $(li).find('span').each(function () {
                        var $span = $(this);
                        var validator = window[$span.attr('id')];
                        var $ctl = $('#{0}'.format(validator.controltovalidate));
                        var visible = $(li).find('span:visible').length > 0;
                        if (visible)
                            $ctl.removeClass('valid');
                        else {
                            $ctl.addClass('valid');
                        }
                    });
                });
            }).on('form-validated-success', function () {
                //$(this).prop('disabled', true);
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeCards" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeftCard" runat="server">
    <ul class="tabs tab-3">
        <li>
            <a href="#"><small>Step 1</small><span>Address</span></a>
        </li>
        <li>
            <a href="#" class="active"><small>Step 2</small><span>Payment</span></a>
        </li>
        <li>
            <a href="#" class=""><small>Step 3</small><span>Complete</span></a>
        </li>
    </ul>
    <div runat="server" id="divPayuError" Visible="False" class="payu-error"></div>
    <asp:Panel class="inner" runat="server" ID="pnlForm">
        <div class="form-overlay"></div>
        <ul>
            <li class="half">
                <input type="button" class="btn btn-blue active" value="Credit Card" />
                <input type="button" class="btn btn-blue" value="Paypal" />
            </li>
            <li>
                <asp:TextBox type="text" class="fa" placeholder="Name of Card holder" runat="server" ID="txtCardholderName" MaxLength="50" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator" ControlToValidate="txtCardholderName" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="50" />
                <i class="fa fa-check"></i>
            </li>
            <li>
                <asp:TextBox type="text" class="fa" placeholder="Card Number" runat="server" ID="txtCardNumber" MaxLength="16" AutoComplete="off" />
                <ui:CreditCardValidator runat="server" CssClass="validator" ControlToValidate="txtCardNumber" ErrorMessage="*" ValidateEmptyText="True" />
                <i class="fa fa-check"></i>
            </li>
            <li class="half">
                <asp:TextBox type="text" class="fa" placeholder="End Date (MMYY)" runat="server" ID="txtEndDate" MaxLength="4" AutoComplete="off" />
                <asp:RegularExpressionValidator runat="server" CssClass="validator left-validator" ControlToValidate="txtEndDate" ValidateEmptyText="True" ErrorMessage="*" ValidationExpression="^[01][0-9]([1][6-9]|[23][0-9])$" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator left-validator" ControlToValidate="txtEndDate" ValidateEmptyText="True" ErrorMessage="*" MinLength="4" MaxLength="4" />
                <i class="fa fa-check"></i>
                <asp:TextBox type="text" class="fa" placeholder="CVC" runat="server" ID="txtCvc" MaxLength="3" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator" ControlToValidate="txtCvc" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="3" />
                <i class="fa fa-check"></i>
            </li>
        </ul>
        <div>
            <asp:Button runat="server" CssClass="btn btn-submit" Text="Stack it" OnClick="btnSubmit_OnClick" ID="btnSubmit" UseSubmitBehavior="False" />
            <p>Next Step <b>Complete</b></p>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphRightCard" runat="server">
    <uc1:ucCheckoutSelectedProduct runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterCards" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphFooterScripts" runat="server">
</asp:Content>
