﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Instack.UI.Project {
    
    
    public partial class Checkout3 {
        
        /// <summary>
        /// SocialShare control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Instack.UI.Project.UserControls.ucSocialShare SocialShare;
        
        /// <summary>
        /// plcRelated control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder plcRelated;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Instack.UI.MasterPages.TwoCards Master {
            get {
                return ((Instack.UI.MasterPages.TwoCards)(base.Master));
            }
        }
    }
}
