﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayU.ExampleWebPages
{
    public enum OrderStatus : int
    {
        New = 0,
        SentToPayu = 1,
        PaymentRejectedFromPayu = 2,
        Success = 3,
        PayuSuccessWithoutOrder = 4
    }
    
    public partial class Order
    {
        public int OrderID { get; set; }
        public Nullable<int> FK_UserID { get; set; }
        public Nullable<int> FK_ProductID { get; set; }
        public Nullable<int> FK_CurrencyID { get; set; }
        public int Quantity { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<decimal> ProductPrice { get; set; }
        public string ProductSize { get; set; }
        public string ProductFabric { get; set; }
        public string ProductImage { get; set; }
        public string ProductName { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public string IpAddress { get; set; }
        public Nullable<int> NthStack { get; set; }
        public Nullable<int> FK_UserAddressID { get; set; }
        public bool IsPaymentApproved { get; set; }
        public OrderStatus Status { get; set; }
        public string ClientIP { get; set; }
        public string ClientUserAgent { get; set; }
        public string PayuRefNumber { get; set; }
        public string CardNameSurname { get; set; }
        public string PayuRequest { get; set; }
        public string PayuResponse { get; set; }
        public Nullable<System.DateTime> PayuResponseDate { get; set; }
        public string Payu3dsResponse { get; set; }

        public virtual Currency Currency { get; set; }
        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
        public virtual UserAddress UserAddress { get; set; }
    }
}
