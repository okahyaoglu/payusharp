﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Instack.Common;
using Instack.Common.Application.UIFormBase;
using Instack.Common.DataAccess;
using Kahia.Common;
using Kahia.Common.Extensions.ConversionExtensions;
using Kahia.Common.Extensions.EnumExtensions;
using Kahia.Common.Extensions.StringExtensions;
using Kahia.Web.ExceptionHandling;
using Kahia.Web.Extensions;
using Kahia.Web.Helpers;
using PayU;
using PayU.AutomaticLiveUpdate;
using PayU.Base;
using BillingDetails = PayU.AutomaticLiveUpdate.BillingDetails;
using Extensions = Instack.Common.Extensions;
using OrderDetails = PayU.AutomaticLiveUpdate.OrderDetails;
using ProductDetails = PayU.AutomaticLiveUpdate.ProductDetails;

namespace Instack.UI.Project
{
    public partial class Checkout2 : UIWebFormBase
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            #region Checkout Page Checks
            OrderManager.DisableCacheAndAutoComplete(this);
            CheckLoginAndRedirectIfNotLoggedIn();
            OrderManager.PrecheckCurrentOrder(this);
            OrderManager.PrecheckOrderStatusAndRedirectIfRequired(this);
            OrderManager.PrecheckOrderUserID(this);
            //todo: state yönetilmeli orderın, payudan cevap beklerken buraya gelirse napcak?
            #endregion
            FormHelper.UpdateValidatorsCommonAttributes(true, ValidatorDisplay.Dynamic, true, OrderManager.ValidationGroup, pnlForm, btnSubmit);
            FormHelper.RegisterValidateAndOverlayScripts(OrderManager.ValidationGroup, GetType(), btnSubmit, ".inner .form-overlay");
            //3ds den hata gelmişse:
            var threeDSErrorCode = ParseQueryString("3DSerrorcode");
            var threeDSError = ParseQueryString("3DSerror");
            if (threeDSErrorCode.IsNotNullAndEmptyString() || threeDSError.IsNotNullAndEmptyString())
            {
                divPayuError.InnerHtml = "<span>3DSecure error occured while making payment. Please check message below and try again.</span><b>{0} ({1})</b>"
                    .FormatString(threeDSError, threeDSErrorCode);
                divPayuError.Visible = true;
            }
        }

        public override string GetVirtualUrl(string langCode)
        {
            return LinkCreator.Checkout2(langCode);
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            Page.Validate(OrderManager.ValidationGroup);
            if (!Page.IsValid)
                return;
            OrderManager.PrecheckCurrentOrder(this); //order boş olamaz artık burada.

            //herşey tamam artık, payu ve db işlemleri
            Order order;
            using (var context = new UIEntities())
            {
                order = OrderManager.FetchOrderFromDb(context);
                if (order.UserAddress == null) //UserAddress boş olamaz artık burada.
                    RedirectTo(LinkCreator.MainPage().AppendQueryString("message", "order-useraddress"), true);
                if (order.Currency == null) //currency boş olamaz artık burada.
                    RedirectTo(LinkCreator.MainPage().AppendQueryString("message", "order-currency"), true);
                if (order.Product == null) //Product boş olamaz artık burada.
                    RedirectTo(LinkCreator.MainPage().AppendQueryString("message", "order-product"), true);
            }

            var cardDetails = PrepareCardDetails();
            var billingDetails = PrepareBillingDetails(order);
            var payuOrder = PayuHelper.PrepareLiveOrder(cardDetails, billingDetails, order, ActiveCurrency);
            
            try
            {
                string xmlResponse;
                var aluResponse = AluRequest.ProcessPayment(payuOrder, out xmlResponse, "");

                var isSuccess = aluResponse.Status == Statuses.SUCCESS;
                var is3DsReponse = aluResponse.Is3DSReponse;
                using (var context = new UIEntities())
                {
                    order = OrderManager.FetchOrderFromDb(context); //burda order_ref null geliyor 3ds_enrolled ise, dolayısıyla payu idye güvenme
                    PrepareOrderPayment(payuOrder, order, ActiveCurrency, aluResponse);
                    //TODO: test et ürünün countu artıyromu gerçekten?
                    if (is3DsReponse)
                    {
                        order.Payu3dsResponse = xmlResponse;
                        order.Status = OrderStatus.SentToPayu;
                    }
                    else if (isSuccess)
                    {
                        order.Product.CountStacked++;
                        order.PayuResponse = xmlResponse;
                        order.IsPaymentApproved = true;
                        order.Status = OrderStatus.Success;
                    }
                    else
                    {
                        order.Status = OrderStatus.PaymentRejectedFromPayu;
                    }
                    context.SaveChanges();
                }

                if (is3DsReponse)
                    RedirectTo(aluResponse.Url3DS, true);

                if (isSuccess)
                {
                    RedirectTo(LinkCreator.Checkout3().ResolveCurrencyUrl(), true);
                }
                else
                {
                    divPayuError.InnerHtml = "<span>An error occured while making payment. Please check message below and try again.</span><b>{0} ({1})</b>"
                        .FormatString(aluResponse.ReturnMessage, aluResponse.ReturnCode);
                    divPayuError.Visible = true;
                }
                //OrderManager.UpdateCurrentOrderToDb();
            }
            catch (PayuException ex)
            {
                divPayuError.InnerHtml = "<span>An error occured while making payment. Please check message below and try again.</span><b>{0} ({1})</b>"
                    .FormatString(ex.Message, "");
                LoggingHelper.InsertLog(new Exception("Payment exception. OrderID:{0}, cardDetails: \"{1}\", billingDetails:\"{2}\"".FormatStringIfParameterNotNull(order.ToString(), cardDetails, billingDetails), ex));
                divPayuError.Visible = true;
            }
        }

        public static void PrepareOrderPayment(OrderDetails payuOrder, Order order, Currency currency, AluResponse aluResponse)
        {
            order.PayuRequest = payuOrder.ToString();
            order.CardNameSurname = payuOrder.CardDetails.CardOwnerName;
            order.PayuRefNumber = aluResponse.RefNo;
            order.PayuResponseDate = Extensions.Now();
        }

        private BillingDetails PrepareBillingDetails(Order order)
        {
            var currentUser = UIAuthManager.Instance.GetCurrentUser();
            var fullNameSplit = currentUser.FullName.SplitAndEliminateEmptyStrings(' ');
            var address = order.UserAddress;
            var result = new BillingDetails
                         {
                             Address = address.Address,
                             Address2 = address.Address2,
                             City = address.City,
                             CountryCode = "TR",
                             Email = currentUser.Email,
                             Fax = "-",
                             FirstName = fullNameSplit.ElementAtOrDefault(0),
                             LastName = fullNameSplit.Skip(1).JoinWith(" "),
                             PhoneNumber = "-",
                             State = "-",
                             ZipCode = address.ZipCode,
                         };
            return result;
        }

        private CardDetails PrepareCardDetails()
        {
            var expireDate = txtEndDate.Text.Trim();
            if (expireDate.Length != 4)
                throw new Exception("Invalid card expire date: {0}".FormatString(expireDate));
            var month = "" + expireDate[0] + expireDate[1];
            var year = "20" + expireDate[2] + expireDate[3];

            var result = new CardDetails
                         {
                             CardNumber = txtCardNumber.Text.Trim(),
                             CardOwnerName = txtCardholderName.Text.Trim(),
                             CVV = txtCvc.Text.Trim(),
                             ExpiryMonth = month,
                             ExpiryYear = year
                         };
            return result;
        }
    }
}