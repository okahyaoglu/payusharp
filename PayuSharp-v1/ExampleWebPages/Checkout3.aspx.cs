﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Instack.Common;
using Instack.Common.Application.UIFormBase;
using Instack.Common.DataAccess;
using Kahia.Common.Extensions.StringExtensions;
using Kahia.Data;
using Kahia.Web.Extensions;
using PayU;

namespace Instack.UI.Project
{
    public partial class Checkout3 : UIWebFormBase
    {
        protected Product RelatedProduct;

        protected Product ProductStacked;

        protected Order CurrentOrder;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            #region Checkout Page Checks
            OrderManager.DisableCacheAndAutoComplete(this);
            CheckLoginAndRedirectIfNotLoggedIn();
            OrderManager.PrecheckCurrentOrder(this);
            #endregion
            using (var context = new UIEntities())
            {
                CurrentOrder = OrderManager.FetchOrderFromDb(context);
                ProductStacked = CurrentOrder.Product;
                var query = context.Products.Include("ProductCategories").IsActiveOrderByDesc();
                var relatedProducts = query.GetRelatedProducts(context, CurrentOrder.Product.GetPkValue());
                RelatedProduct = relatedProducts.FirstOrDefault();
            }
            if (RelatedProduct != null)
                plcRelated.DataBind();
            //ShoppingCart.Item = null;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            SocialShare.URL = LinkCreator.ProductDetail(ProductStacked).GetFullResolvedUrlWithPort();
            SocialShare.PageTitle = "Stacked {0} of {1}! {2}".FormatString(ProductStacked.CountStacked, ProductStacked.CountRequired, MetaBuilder.PreparePageTitle(ProductStacked));
            OrderManager.CurrentOrderID = null;
        }

        public override string GetVirtualUrl(string langCode)
        {
            return LinkCreator.Checkout3(langCode);
        }
    }
}