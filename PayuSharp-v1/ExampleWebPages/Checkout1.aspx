﻿<%@ Page HtmlCssClass="Checkout1-page Checkout-page" MasterPageFile="~/MasterPages/TwoCards.master" CodeBehind="Checkout1.aspx.cs" Inherits="Instack.UI.Project.Checkout1" AutoEventWireup="true" Language="C#" %>

<%@ MasterType VirtualPath="~/MasterPages/TwoCards.master" %>
<%@ Register Src="~/Project/UserControls/ucCheckoutSelectedProduct.ascx" TagPrefix="uc1" TagName="ucCheckoutSelectedProduct" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        //valid classı gelmeli validse textbox
        var checkoutPanel = '#<%=pnlForm.ClientID%>';
        var checkoutBtn = '#<%=btnSubmit.ClientID%>';
        $(function () {
            tools.inputFilters.allowOnlyNumbers('#<%=txtPhone.ClientID%>');
            $(checkoutBtn).on('form-validated', function () {
                $('ul li', checkoutPanel).each(function (index, li) {
                    $(li).find('span').each(function () {
                        var $span = $(this);
                        var validator = window[$span.attr('id')];
                        var $ctl = $('#{0}'.format(validator.controltovalidate));
                        var visible = $(li).find('span:visible').length > 0;
                        if (visible)
                            $ctl.removeClass('valid');
                        else {
                            $ctl.addClass('valid');
                        }
                    });
                });
            }).on('form-validated-success', function () {
                //$(this).prop('disabled', true);
            });
            $('#<%=ddlAddress.ClientID%>').change(function () {
                if ($(this).val() == "") {
                    $('#<%=pnlForm.ClientID%>').show('fade', {}, 'fast');
                    $('#<%=btnUseThisAddress.ClientID%>').hide('fade', {}, 'fast');
                } else {
                    $('#<%=pnlForm.ClientID%>').hide('fade', {}, 'fast');
                    $('#<%=btnUseThisAddress.ClientID%>').show('fade', {}, 'fast');
                }
            }).trigger('change');
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeCards" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeftCard" runat="server">
    <ul class="tabs tab-3">
        <li>
            <a href="#" class="active"><small>Step 1</small><span>Address</span></a>
        </li>
        <li>
            <a href="#" class=""><small>Step 2</small><span>Payment</span></a>
        </li>
        <li>
            <a href="#" class=""><small>Step 3</small><span>Complete</span></a>
        </li>
    </ul>
    <div class="inner">
        <asp:DropDownList runat="server" ID="ddlAddress" />
        <asp:Button runat="server" ID="btnUseThisAddress" UseSubmitBehavior="False" CssClass="btn btn-submit" Text="Send to this Address" OnClick="btnUseThisAddress_OnClick" />
    </div>
    <asp:Panel class="inner" runat="server" ID="pnlForm">
        <ul>
            <li class="half">
                <asp:TextBox type="text" class="fa" placeholder="Firstname" runat="server" ID="txtFirstName" MaxLength="50" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator left-validator" ControlToValidate="txtFirstName" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="50" />
                <i class="fa fa-check"></i>
                <asp:TextBox type="text" class="fa" placeholder="Lastname" runat="server" ID="txtLastName" MaxLength="50" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator" ControlToValidate="txtLastName" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="50" />
                <i class="fa fa-check"></i>
            </li>
            <li>
                <asp:TextBox type="text" class="fa" placeholder="Address Line 1" runat="server" ID="txtAddress1" MaxLength="100" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator" ControlToValidate="txtAddress1" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="100" />
                <i class="fa fa-check"></i>
            </li>
            <li>
                <asp:TextBox type="text" class="fa" placeholder="Address Line 2" runat="server" ID="txtAddress2" MaxLength="100" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator" ControlToValidate="txtAddress2" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="100" />
                <i class="fa fa-check"></i>
            </li>
            <li class="half">
                <asp:TextBox type="text" class="fa" placeholder="City" runat="server" ID="txtCity" MaxLength="50" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator left-validator" ControlToValidate="txtCity" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="50" />
                <i class="fa fa-check"></i>
                <asp:TextBox type="text" class="fa" placeholder="Zip code" runat="server" ID="txtZipCode" MaxLength="50" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator" ControlToValidate="txtZipCode" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="50" />
                <i class="fa fa-check"></i>
            </li>
            <li>
                <asp:TextBox type="text" class="fa" placeholder="Phone number" runat="server" ID="txtPhone" MaxLength="100" AutoComplete="off" />
                <ui:MinMaxLengthValidator runat="server" CssClass="validator" ControlToValidate="txtPhone" ValidateEmptyText="True" ErrorMessage="*" MinLength="3" MaxLength="100" />
                <i class="fa fa-check"></i>
            </li>
        </ul>
        <div>
            <asp:Button runat="server" ID="btnSubmit" UseSubmitBehavior="False" CssClass="btn btn-submit" Text="Send to this Address" OnClick="btnSubmit_OnClick" />
        </div>
    </asp:Panel>
    <p class="next-step">Next Step <b>Payment</b></p>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphRightCard" runat="server">
    <uc1:ucCheckoutSelectedProduct runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterCards" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphFooter" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphFooterScripts" runat="server">
</asp:Content>
