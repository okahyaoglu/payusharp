﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using PayU.Base;

namespace PayU
{
    public class MarketPlaceParameterHandler : ParameterHandler
    {
        const string ORDER_HASH_PARAMETER_NAME = "signature";
        private bool AppendTimestamp;
        public MarketPlaceParameterHandler(Dictionary<string, string> parameters, DateTime timestamp, bool appendTimestamp = true)
            : base(parameters, timestamp, true)
        {
            AppendTimestamp = appendTimestamp;
        }

        protected override string GetHashString()
        {
            //ORDER_HASH 'i hiçbir koşulda hashstring'e eklememeli
            var str = new StringBuilder();
            var parameters = Parameters.Where(p => p.Key != "timestamp" && p.Key != ORDER_HASH_PARAMETER_NAME);

            var usedParameters = parameters
                .SelectMany(parameter => parameter);
                //.Where(p => p.Value != null && !p.ExcludeFromHash);

            foreach (var parameterValue in usedParameters)
            {
                str.Append(parameterValue); //.Value
            }

            //https://secure.payu.com.tr/docs/marketplace/ timestamp en sonuna eklenecek
            if (AppendTimestamp)
                str.Append(base._timestamp.ToUnixTimestamp());

            var result = str.ToString();
            return result;
        }

        protected override string CalculateSignedHashString()
        {
            return HashString.HashWithSignatureHmacSha256();
        }
    }
}
