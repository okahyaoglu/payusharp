﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Script.Serialization;
using Kahia.Web.Extensions;
using PayU.Base;

namespace PayU.MarketPlaceApi
{
    public class MarketPlaceGetRequest : PayuRequestBase<MarketPlaceGetRequest.Response>
    {
        public class Response : PayuResponseBase
        {
            public MerchantInfoResult SellerResponse { get; set; }
        }

        public class MerchantInfoResult
        {
            public string sellerCode { get; set; }
            public string accountBalance { get; set; }
            public string kycStatus { get; set; }
            public string merchant { get; set; }
            public string sellerType { get; set; }
            public string registrationNumber { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string zip { get; set; }
            public string countryCode { get; set; }
            public string phone { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string email { get; set; }
            public string automaticEmail { get; set; }
            public string fiscalCode { get; set; }
        }

        private class JsonResult
        {
            public MerchantInfoResult seller { get; set; }
        }

        private string SubMerchantKey;

        public MarketPlaceGetRequest(string subMerchantKey, string merchant)
            : base(new Dictionary<string, string> { { "merchant", merchant }, { "timestamp", DateTime.UtcNow.ToUnixTimestamp().ToString() } })
        {
            SubMerchantKey = subMerchantKey;
        }

        protected override void CreateParameterHandler(Dictionary<string, string> parameters)
        {
            ParameterHandler = new MarketPlaceParameterHandler(parameters, TimeStampDate);
        }


        protected override Response CreatePayuResponse()
        {
            var result = new Response
            {
                RequestURL = ("https://secure.payu.com.tr/api/marketplace/v1/seller/" + SubMerchantKey),
                //.AppendQueryString("timestamp", TimeStamp)
                //.AppendQueryString("merchant", Merchant),
                PostedValues = ParameterHandler.GetRequestData(),
                RequestMethod = HttpMethod.Get,
                HashString = ParameterHandler.HashString
            };
            result.RequestURL = result.RequestURL.AppendQueryString("signature", ParameterHandler.SignedHashString);
            return result;
        }

        protected override void ProcessRequest(Response result)
        {
            base.ProcessRequest(result);
            if (result.ResponseString == null)
                return;

            var serializer = new JavaScriptSerializer();
            var json = serializer.Deserialize<JsonResult>(result.ResponseString);
            result.SellerResponse = json.seller;
        }
    }
}
