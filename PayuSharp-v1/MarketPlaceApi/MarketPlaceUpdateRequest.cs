﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using PayU.Base;

namespace PayU.MarketPlaceApi
{
    public class MarketPlaceUpdateRequest : PayuRequestBase<MarketPlaceUpdateRequest.Response>
    {
        private readonly string _merchantName;

        public class Response : PayuResponseBase
        {
            public string submerchant { get; set; }
            public string responseCode { get; set; }
            public string statusCode { get; set; }
            public string errors { get; set; }

            public override string ToString()
            {
                return "submerchant: " + submerchant + "\n" + base.ToString();
            }
        }

        /*
{
  "meta": {
    "status": {
      "code": 3000,
      "message": "Validation errors"
    },
    "response": {
      "httpCode": 400,
      "httpMessage": "400 Bad Request"
    }
  },
  "errors": {
    "Email": "There is already an user with this email address!"
  }
}         
         * 
{
  "meta": {
    "status": {
      "code": 0,
      "message": "success"
    },
    "response": {
      "httpCode": 200,
      "httpMessage": "200 OK"
    }
  },
  "sellerCode": "5WM5ZTS1"
}
         */
        /// <summary>
        /// FiscalCode: TCKimlik
        /// RegistrationNumber: IBAN
        /// </summary>
        public MarketPlaceUpdateRequest(string merchantName, string parentmerchantName, string sellerType, string registrationNumber, string address, string city, string state, string zip, string countryCode,
            string phone, string firstName, string lastName, string email, string automaticEmail, string fiscalCode)
            : base(null)
        {
            _merchantName = merchantName;
            if (string.IsNullOrEmpty(registrationNumber) || registrationNumber.Length <= 20)
                throw new Exception("registrationNumber boş olamaz. IBAN numarası set edilmeli");
            Parameters["merchant"] = parentmerchantName; 
            Parameters["sellerType"] = sellerType; 
            Parameters["registrationNumber"] = registrationNumber.Replace(" ", ""); 
            Parameters["address"] = address; 
            Parameters["city"] = city; 
            Parameters["city"] = city; 
            Parameters["state"] = state; 
            Parameters["zip"] = zip; 
            Parameters["countryCode"] = countryCode; 
            Parameters["phone"] = phone; 
            Parameters["firstName"] = firstName; 
            Parameters["lastName"] = lastName; 
            Parameters["email"] = email; 
            Parameters["automaticEmail"] = automaticEmail; 
            Parameters["fiscalCode"] = fiscalCode; 
        }

        protected override void CreateParameterHandler(Dictionary<string, string> parameters)
        {
            ParameterHandler = new MarketPlaceParameterHandler(parameters, TimeStampDate);
        }

        protected override Response CreatePayuResponse()
        {
            var result = new Response
            {
                RequestURL = "https://secure.payu.com.tr/api/marketplace/v1/seller/"+_merchantName,
                //.AppendQueryString("timestamp", TimeStamp)
                //.AppendQueryString("merchant", Merchant),
                PostedValues = ParameterHandler.GetRequestData(),
                RequestMethod = HttpMethod.Post,
                HashString = ParameterHandler.HashString
            };
            //result.RequestURL = result.RequestURL.AppendQueryString("signature", ParameterHandler.SignedHashString);
            result.PostedValues.Add("signature", ParameterHandler.SignedHashString);
            result.PostedValues.Add("timestamp", TimeStamp);
            return result;
        }

        protected override void ProcessRequest(Response result)
        {
            base.ProcessRequest(result);
            if (result.ResponseString == null)
                return;
            
            dynamic parsed = JObject.Parse(result.ResponseString);
            result.errors = parsed.errors != null ? parsed.errors.ToString() : "";
            result.submerchant = parsed.sellerCode;
            result.responseCode = parsed.meta.response.httpCode;
            result.statusCode = parsed.meta.status.code;
        }
    }
}
