﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using PayU.Base;

namespace PayU.MarketPlaceApi
{
    public class MarketPlaceCreateRequest : PayuRequestBase<MarketPlaceCreateRequest.Response>
    {
        public class Response : PayuResponseBase
        {
            public string submerchant { get; set; }
            public string responseCode { get; set; }
            public string statusCode { get; set; }
            public string errors { get; set; }

            public override string ToString()
            {
                return "submerchant: " + submerchant + "\n" + base.ToString();
            }
        }

        /*
{
  "meta": {
    "status": {
      "code": 3000,
      "message": "Validation errors"
    },
    "response": {
      "httpCode": 400,
      "httpMessage": "400 Bad Request"
    }
  },
  "errors": {
    "Email": "There is already an user with this email address!"
  }
}         
         * 
{
  "meta": {
    "status": {
      "code": 0,
      "message": "success"
    },
    "response": {
      "httpCode": 200,
      "httpMessage": "200 OK"
    }
  },
  "sellerCode": "5WM5ZTS1"
}
         */
        /// <summary>
        /// FiscalCode: TCKimlik
        /// RegistrationNumber: IBAN
        /// </summary>
        public MarketPlaceCreateRequest(string parentMerchant, string sellerType, string registrationNumber, string address, string city, string state, string zip, string countryCode,
            string phone, string firstName, string lastName, string email, string automaticEmail, string fiscalCode)
            : base(null)
        {
            //if (string.IsNullOrEmpty(fiscalCode) || fiscalCode.Length != 11)
                //throw new Exception("fiscalCode boş olamaz. TC Kimlik no set edilmeli");
            if (string.IsNullOrEmpty(registrationNumber) || registrationNumber.Length <= 20)
                throw new Exception("registrationNumber boş olamaz. IBAN numarası set edilmeli");
            Parameters["merchant"] = parentMerchant; //.ToValueHolder();
            Parameters["sellerType"] = sellerType; //.ToValueHolder();
            Parameters["registrationNumber"] = registrationNumber.Replace(" ", ""); //.ToValueHolder();
            Parameters["address"] = address; //.ToValueHolder();
            Parameters["city"] = city; //.ToValueHolder();
            Parameters["city"] = city; //.ToValueHolder();
            Parameters["state"] = state; //.ToValueHolder();
            Parameters["zip"] = zip; //.ToValueHolder();
            Parameters["countryCode"] = countryCode; //.ToValueHolder();
            Parameters["phone"] = phone; //.ToValueHolder();
            Parameters["firstName"] = firstName; //.ToValueHolder();
            Parameters["lastName"] = lastName; //.ToValueHolder();
            Parameters["email"] = email; //.ToValueHolder();
            Parameters["automaticEmail"] = automaticEmail; //.ToValueHolder();
            Parameters["fiscalCode"] = fiscalCode; //.ToValueHolder();
        }

        protected override void CreateParameterHandler(Dictionary<string, string> parameters)
        {
            ParameterHandler = new MarketPlaceParameterHandler(parameters, TimeStampDate);
        }


        protected override Response CreatePayuResponse()
        {
            var result = new Response
            {
                RequestURL = "https://secure.payu.com.tr/api/marketplace/v1/seller",
                //.AppendQueryString("timestamp", TimeStamp)
                //.AppendQueryString("merchant", Merchant),
                PostedValues = ParameterHandler.GetRequestData(),
                RequestMethod = HttpMethod.Post,
                HashString = ParameterHandler.HashString
            };
            //result.RequestURL = result.RequestURL.AppendQueryString("signature", ParameterHandler.SignedHashString);
            result.PostedValues.Add("signature", ParameterHandler.SignedHashString);
            result.PostedValues.Add("timestamp", TimeStamp);
            return result;
        }

        protected override void ProcessRequest(Response result)
        {
            base.ProcessRequest(result);
            if (result.ResponseString == null)
                return;
            
            dynamic parsed = JObject.Parse(result.ResponseString);
            result.errors = parsed.errors != null ? parsed.errors.ToString() : "";
            result.submerchant = parsed.sellerCode;
            result.responseCode = parsed.meta.response.httpCode;
            result.statusCode = parsed.meta.status.code;
        }
    }
}
