using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Kahia.Common.Extensions.GeneralExtensions;

namespace PayU
{
    public static class HashHelper
    {
        public static readonly string SignatureKey = ConfigurationManager.AppSettings["Payu.Secret"].ThrowIfNullOrEmptyString("Payu.Secret AppSetting'i bulunamad�. Hashing i�in gereken key.");

        public static string HashWithSignatureHmacMd5(this string hashString)
        {
            var binaryHash = new HMACMD5(Encoding.UTF8.GetBytes(SignatureKey))
                .ComputeHash(Encoding.UTF8.GetBytes(hashString));
            
            var hash = BitConverter.ToString(binaryHash)
                .Replace("-", string.Empty)
                    .ToLowerInvariant();

            return hash;
        }

        public static string HashWithSignatureHmacSha256(this string hashString)
        {
            var binaryHash = new HMACSHA256(Encoding.UTF8.GetBytes(SignatureKey))
                .ComputeHash(Encoding.UTF8.GetBytes(hashString));
            
            var hash = BitConverter.ToString(binaryHash)
                .Replace("-", string.Empty)
                    .ToLowerInvariant();

            return hash;
        }
    }
}

