﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PayU
{
    public class LoggingHandler : DelegatingHandler
    {
        private readonly StringBuilder _stringToLog;

        public LoggingHandler(HttpMessageHandler innerHandler, StringBuilder stringToLog)
            : base(innerHandler)
        {
            _stringToLog = stringToLog;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            _stringToLog.AppendLine("Request:");
            _stringToLog.AppendLine(request.ToString());
            if (request.Content != null)
            {
                _stringToLog.AppendLine(await request.Content.ReadAsStringAsync());
            }
            _stringToLog.AppendLine();

            var response = await base.SendAsync(request, cancellationToken);

            _stringToLog.AppendLine("Response:");
            _stringToLog.AppendLine(response.ToString());
            if (response.Content != null)
            {
                _stringToLog.AppendLine(await response.Content.ReadAsStringAsync());
            }
            _stringToLog.AppendLine();

            return response;
        }
    }
}
