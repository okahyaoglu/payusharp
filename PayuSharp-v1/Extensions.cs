﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kahia.Common;
using Kahia.Common.Extensions;
using Kahia.Common.Extensions.StringExtensions;
using PayU.AutomaticLiveUpdate;
using PayU.Base;

namespace PayU
{
    public static class Extensions
    {
        public static string GetMaskedCardInfo(this CardDetails cardInfo)
        {
            if (cardInfo == null)
                return null;
            var cardNoMasked = new String(cardInfo.CardNumber.Select((c, i) => i >= 12 || i < 4 ? c : '*').ToArray());
            return cardNoMasked;
        }

        public static void SetBillingDetails(this IPayuTrxParameters entity, PayU.Base.BillingDetails billingDetails)
        {
            entity.BILL_ADDRESS = billingDetails.Address;
            entity.BILL_ADDRESS2 = billingDetails.Address2;
            entity.BILL_CITY = billingDetails.City;
            entity.BILL_COUNTRYCODE = billingDetails.CountryCode;
            entity.BILL_EMAIL = billingDetails.Email;
            entity.BILL_FAX = billingDetails.Fax;
            entity.BILL_FNAME = billingDetails.FirstName;
            entity.BILL_LNAME = billingDetails.LastName;
            entity.BILL_PHONE = billingDetails.PhoneNumber;
            entity.BILL_STATE = billingDetails.State;
            entity.BILL_ZIPCODE = billingDetails.ZipCode;
        }


        public static Dictionary<string, string> ToValueHolderCollection(this Dictionary<string, string> parameter)
        {
            var parameters = parameter.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            return parameters;
        }


        internal static Dictionary<string, string> GetPropertyValuesAsValueHolderCollection(this object parameter, Type type)
        {
            //objenin property'leri değil verilen tipin property'leri üzerinden işlem yapar.
            var properties = type.GetProperties().Where(p => !Attribute.IsDefined(p, typeof(ParameterHandler.IgnoreAttribute)));
            return properties.ToDictionary(p => p.Name, p => p.GetValue(parameter).ToStringByDefaultValue());
        }


        public static string ToPayuString(this DateTime date)
        {
            var timestamp = date.ToString("yyyy-MM-dd HH:mm:ss");
            return timestamp;
        }

        public static long ToUnixTimestamp(this DateTime date)
        {
            return Convert.ToInt64(date.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
        }

        public static Dictionary<string, string> ExcludeCardParameters(this Dictionary<string, string> parameter)
        {
            var protectedParameters = new[] { "CC_CVV", "CC_NUMBER", "CC_OWNER", "EXP_MONTH", "EXP_YEAR" };
            var result = parameter.ToDictionary(kvp => kvp.Key, kvp => protectedParameters.Contains(kvp.Key) ? "***" : kvp.Value);
            return result;
        }

        internal static string RoundString(this decimal value)
        {
            return Math.Round(value,2, MidpointRounding.AwayFromZero).ToString("0.##", StaticCultures.ENUS);
        }
    }
}
