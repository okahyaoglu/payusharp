﻿namespace PayUSharpRefund
{
    /// <summary>
    /// FiscalCode: TCKimlik
    /// RegistrationNumber: IBAN
    /// </summary>
    public interface IPayuTrxParameters
    {
        string MERCHANT { get; set; }
        string BACK_REF { get; set; }
        string CLIENT_IP { get; set; }
        string ORDER_REF { get; set; }
        string PRICES_CURRENCY { get; set; }
        string SELECTED_INSTALLMENTS_NUMBER { get; set; }
        string BILL_ADDRESS { get; set; }
        string BILL_ADDRESS2 { get; set; }
        string BILL_CITY { get; set; }
        string BILL_COUNTRYCODE { get; set; }
        string BILL_EMAIL { get; set; }
        string BILL_FAX { get; set; }
        string BILL_FNAME { get; set; }
        string BILL_LNAME { get; set; }
        string BILL_PHONE { get; set; }
        string BILL_STATE { get; set; }
        string BILL_ZIPCODE { get; set; }
        string CC_TOKEN { get; set; }
    }
}