using System;

namespace PayUSharpRefund
{
    public class PayuException: ApplicationException
    {
        public PayuException (string message, Exception innerException): base(message, innerException)
        {
        }
    }
}

