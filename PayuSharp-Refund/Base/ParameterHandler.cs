﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PayUSharpRefund.Base
{
    public class ParameterHandler
    {
        /// <summary>
        /// ValueHolderCollection'a çevirilirken ignore edilmesini sağlar o propertynin
        /// </summary>
        public class IgnoreAttribute : Attribute
        {
        }

        protected readonly DateTime _timestamp;

        //public class ValueHolder
        //{
        //    public ValueHolder()
        //    {

        //    }

        //    public ValueHolder(string value)
        //    {
        //        Value = value;
        //    }
        //    public string Value { get; set; }
        //    internal bool ExcludeFromHash { get; set; }
        //    internal int SortIndex { get; set; }

        //    public override string ToString()
        //    {
        //        return Value;
        //    }
        //}

        protected ILookup<string, string> Parameters { get; set; }
        public string HashString { get; protected set; }
        public string SignedHashString { get; protected set; }

        public ParameterHandler(Dictionary<string, string> parameters, DateTime timestamp, bool sort = true)
        {
            _timestamp = timestamp;

            if (sort)
            {
                Parameters = parameters.OrderBy(pair => pair.Key, StringComparer.InvariantCultureIgnoreCase).ToLookup(pair => pair.Key, pair => pair.Value);
            }
            else
            {
                //Refund Request sort etmedne kullanıyor ondan ötürü false ise dokunmamalı parametrelere
                Parameters = parameters.ToLookup(pair => pair.Key, pair => pair.Value);
            }
        }

        //public ParameterHandler(object o, bool sort = true)
        //{
        //    var parameters = ParametersFromType(o, "", false);

        //    if (sort)
        //    {
        //        parameters = parameters.OrderBy(pair => pair.Key);
        //    }
        //    else
        //    {
        //        parameters = parameters.OrderBy(pair => pair.Value.SortIndex);
        //    }

        //    Parameters = parameters.ToLookup(pair => pair.Key, pair => pair.Value);
        //}

        public static Dictionary<string, string> ParametersFromType(object o, string suffix = "") //, bool excludeFromHash = false
        {
            if (o is Dictionary<string, string>)
                return (Dictionary<string, string>) o;

            var itemType = o.GetType();

            if (itemType.IsGenericType &&
                itemType.GetGenericTypeDefinition() == typeof(List<>))
            {
                return ParametersFromListType(o as IList); //, excludeFromHash
            }

            var result = itemType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .SelectMany(property => ParametersFromType(o, property, suffix)); //, excludeFromHash
            return result.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        private static Dictionary<string, string> ParametersFromListType(IList list) //
        {
            var dictionary = new Dictionary<string, string>();

            for (var idx = 0; idx < list.Count; idx++)
            {
                var temp = ParametersFromType(list[idx], string.Format("[{0}]", idx)); //, excludeFromHash
                foreach (var kvp in temp)
                {
                    dictionary.Add(kvp.Key, kvp.Value);
                }
            }
            return dictionary;
        }

        private static Dictionary<string, string> ParametersFromType(object o, PropertyInfo property, string suffix) //, bool excludeFromHash
        {
            var attributes = property.GetCustomAttributes(false)
                .Where(attr => attr is ParameterAttribute)
                .Cast<ParameterAttribute>()
                .ToList();

            var result = new Dictionary<string, string>();
            if (!attributes.Any())
                return result;
            var attribute = attributes.First();

            var value = property.GetValue(o, new object[] {});

            if (value == null) return result;

            if (attribute.IsNested)
                return ParametersFromType(value, ""); //, attribute.ExcludeFromHash
            var parameterFromValue = ParameterFromValue(suffix, attribute, value); //excludeFromHash
            result.Add(parameterFromValue.Key, parameterFromValue.Value);
            return result;
        }

        private static KeyValuePair<string, string> ParameterFromValue(string suffix, ParameterAttribute attribute, object value) //, bool excludeFromHash)
        {
            return new KeyValuePair<string, string>(
                attribute.Name + suffix, value.ToString()
                //new ValueHolder
                //    {
                //        Value = GetStringValue(attribute, value),
                //        ExcludeFromHash = excludeFromHash || attribute.ExcludeFromHash,
                //        SortIndex = attribute.SortIndex
                //    }
            );
        }

        private static string GetStringValue(ParameterAttribute attribute, object value)
        {
            if (value is DateTime)
            {
                value = ((DateTime) value).ToUniversalTime();
            }
            else if (value is Boolean)
            {
                value = ((Boolean) value).GetHashCode();
            }
            return string.Format(CultureInfo.InvariantCulture, attribute.FormatString ?? "{0}", value);
        }

        public Dictionary<string, string> GetRequestData()
        {
            var data = new Dictionary<string, string>();
            foreach (var parameter in Parameters)
            {
                foreach (var parameterValue in parameter)
                {
                    //if (parameterValue.ExcludeFromHash)
                    //    continue;
                    data.Add(parameter.Key, parameterValue);
                }
            }
            return data;
        }

        protected virtual string GetHashString()
        {
            //ORDER_HASH 'i hiçbir koşulda hashstring'e eklememeli
            var str = new StringBuilder();
            var usedParameters = Parameters
                .SelectMany(parameter => parameter);
            //.Where(p => p.Value != null && !p.ExcludeFromHash);

            foreach (var parameterValue in usedParameters)
            {
                str.Append(Encoding.UTF8.GetByteCount(parameterValue));
                str.Append(parameterValue);
            }


            var result = str.ToString();
            return result;
        }

        public virtual void CreateOrderRequestHash()
        {
            HashString = GetHashString();
            SignedHashString = CalculateSignedHashString();
        }

        protected virtual string CalculateSignedHashString()
        {
            return HashString.HashWithSignatureHmacMd5();
        }
    }
}