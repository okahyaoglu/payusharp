﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Kahia.Common.Extensions.CollectionExtensions;
using Kahia.Common.Extensions.StringExtensions;
using PayUSharpRefund.Base;

namespace PayUSharpRefund.RefundAPI
{
    public class RefundRequest : PayuRequestBase<RefundRequest.Response>
    {
        public class Response : PayuResponseBase
        {
            public Dictionary<string, string> Parameters { get; set; }
            public bool Success { get; set; }
            public string ResponseCode { get; set; }

            public override string ToString()
            {
                return "Response Parameters: " + Parameters.GetCollectionString() + "\n" + base.ToString();
            }
        }

        public RefundRequest(string merchant, string payuOrderRef, decimal orderAmount, string currency)
            : base(null) 
        {
            if (merchant == null)
                throw new ArgumentNullException("merchant");
            if(payuOrderRef == null)
                throw new ArgumentNullException("payuOrderRef");
            if (currency == null)
                throw new ArgumentNullException("currency");

            Parameters["MERCHANT"] = merchant; //.ToValueHolder();
            Parameters["ORDER_REF"] = payuOrderRef; //.ToValueHolder();
            Parameters["ORDER_AMOUNT"] = orderAmount.RoundString(); //.ToValueHolder();
            Parameters["ORDER_CURRENCY"] = currency; //.ToValueHolder();
            Parameters["IRN_DATE"] = DateTime.UtcNow.ToPayuString(); //.ToValueHolder();
            Parameters["ORDER_HASH"] = ""; //.ToValueHolder();
            Parameters["AMOUNT"] = orderAmount.RoundString(); //.ToValueHolder();
            //Parameters["LOYALTY_POINTS_AMOUNT"] = amountStr; //.ToValueHolder();
            //if (subMerchantKey != "")
            //{
            //    Parameters["ORDER_MPLACE_MERCHANT[0]"] = subMerchantKey; //.ToValueHolder();
            //    Parameters["ORDER_MPLACE_AMOUNT[0]"] = amountStr; //.ToValueHolder();
            //}
        }

        protected override void CreateParameterHandler(Dictionary<string, string> parameters)
        {
            ParameterHandler = new RefundParameterHandler(parameters, TimeStampDate);
        }

        protected override Response CreatePayuResponse()
        {
            var response = base.CreatePayuResponse();
            response.RequestURL = "https://secure.payu.com.tr/order/irn.php";
            response.PostedValues["ORDER_HASH"] = ParameterHandler.SignedHashString;
            return response;
        }

        protected override void ProcessRequest(Response result)
        {
            base.ProcessRequest(result);
            if (result.ResponseString == null)
                return;

            var xdoc = XDocument.Parse(result.ResponseString);
            var _dictionary = xdoc.Descendants().ToDictionary(d => d.Name.LocalName, d => d.Value);
            result.Parameters = _dictionary;
            //27285213|7|OK|2017-02-17 18:02:01|62b8c068088cd4f2c60eae6b720e469d
            result.ResponseCode = result.ResponseString.ToStringByDefaultValue().Split('|').ElementAtOrDefault(2);
            result.Success = result.ResponseCode == "OK";
        }

    }
}
