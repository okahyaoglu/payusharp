﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayUSharpRefund.Base;

namespace PayUSharpRefund.RefundAPI
{
    public class RefundParameterHandler : ParameterHandler
    {
        public RefundParameterHandler(Dictionary<string, string> parameters, DateTime timestamp)
            : base(parameters,timestamp, false)
        {

        }

        protected override string GetHashString()
        {
            //ORDER_HASH 'i hiçbir koşulda hashstring'e eklememeli
            var str = new StringBuilder();
            var parameters = Parameters.Where(p => p.Key != "ORDER_HASH");

            var usedParameters = parameters
                .SelectMany(parameter => parameter)
                .Where(p => p != null); //&& p.Value != null && !p.ExcludeFromHash

            foreach (var parameterValue in usedParameters)
            {
                str.Append(parameterValue.Length);//.Value
                str.Append(parameterValue); //.Value
            }
            //str.Append(base._timestamp.ToUnixTimestamp());

            var result = str.ToString();
            return result;
        }

        protected override string CalculateSignedHashString()
        {
            return HashString.HashWithSignatureHmacMd5();
        }
    }
}
